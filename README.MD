# Execution order

* Open three different terminals 
* Start the proxy service with the following command ``cargo run -- -c 0.0.0.0:1212 -s 127.0.0.1:1313``
* Use netcat to listen to tcp connections ``nc -l 1313``
* In the final terminal make a connection to 1212 using ``echo hello | nc 127.0.0.1 1212``

