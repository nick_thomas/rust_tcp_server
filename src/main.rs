use clap::{ App, Arg};
use tokio::{io, net::{TcpStream, TcpListener}, select};

#[tokio::main]
async fn main()-> io::Result<()> {
    let matches = App::new("proxy")
        .version("0.1")
        .author("Nick Tom Thomas <nick@nick.com>")
        .about("A simple tcp proxy")
        .arg(
            Arg::with_name("client")
            .short("c")
            .long("client")
            .value_name("ADDRESS")
            .help("The address of the eyeball that we will be proxy traffic for")
            .takes_value(true)
            .required(true),
        ).arg(
        Arg::with_name("server")
        .short("s")
        .long("server")
        .value_name("ADDRESS")
        .help("The address of the origin that we will be proxying traffic for")
        .takes_value(true)
        .required(true),
        ).get_matches();

    let client = matches.value_of("client").unwrap();
    let server = matches.value_of("server").unwrap();

    proxy(client, server).await;
    Ok(())
}

async fn proxy(client: &str, server: &str) -> io::Result<()> {
    // Listen for connections from eyeball and forward to the origin.

    let listener = TcpListener::bind(client).await?;
    loop {
        let (eyeball, _) = listener.accept().await?;
        let origin = TcpStream::connect(server).await?;
        let (mut eread, mut ewrite) = eyeball.into_split();
        let (mut oread, mut owrite) = origin.into_split();

        let e2o = tokio::spawn(async move { io::copy(&mut eread,&mut owrite ).await });
        let o2e = tokio::spawn(async move { io::copy(&mut oread, &mut ewrite).await });

        select! {
            _ = e2o => println!("e2o done"),
            _ = o2e => println!("o2e done"),
        }

    }

}
